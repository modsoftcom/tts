﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TTS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private Exception exception;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }



        protected void Application_Error(object sender, EventArgs e)
        {
            exception = Server.GetLastError();
            var httpContext = ((HttpApplication)sender).Context;
            httpContext.Response.Clear();
            httpContext.ClearError();

            if (new HttpRequestWrapper(httpContext.Request).IsAjaxRequest())
            {
                return;
            }
            ExecuteErrorController(httpContext, exception as HttpException);
        }

        private void ExecuteErrorController(HttpContext httpContext, HttpException exception)
        {
            if (exception != null && exception.GetHttpCode() == (int)HttpStatusCode.NotFound)
            {
                Session["PageEx"] = this.exception;
                httpContext.ClearError();
                httpContext.Response.RedirectToRoute("hata-sayfasi");
            }
            else
            {
                Session["PageEx"] = this.exception;
                httpContext.ClearError();
                httpContext.Response.RedirectToRoute("sunucu-hatasi");
            }
        }
    }
}
