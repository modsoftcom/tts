﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Drawing;

namespace TTS.Web
{
    public static class Utils
    {
        public static class Cookies
        {
            public static void Save(Controller Controller, string key, object entity)
            {
                HttpCookie cookie = Controller.Request.Cookies[key];
                if (cookie != null)
                    cookie.Value = JsonConvert.SerializeObject(entity);
                else
                {
                    cookie = new HttpCookie(key);
                    cookie.Value = JsonConvert.SerializeObject(entity);
                    cookie.Expires = DateTime.Now.AddYears(1);
                }
                Controller.Response.Cookies.Add(cookie);
            }
            public static void Save(Controller Controller, string key, string value)
            {
                HttpCookie cookie = Controller.Request.Cookies[key];
                if (cookie != null)
                    cookie.Value = value;
                else
                {
                    cookie = new HttpCookie(key);
                    cookie.Value = value;
                    cookie.Expires = DateTime.Now.AddYears(1);
                }
                Controller.Response.Cookies.Add(cookie);
            }
            public static void Save(HttpContextBase Context, string key, string value)
            {
                HttpCookie cookie = Context.Request.Cookies[key];
                if (cookie != null)
                    cookie.Value = value;
                else
                {
                    cookie = new HttpCookie(key);
                    cookie.Value = value;
                    cookie.Expires = DateTime.Now.AddYears(1);
                }
                Context.Response.Cookies.Add(cookie);
            }
            public static string Get(Controller Controller, string key)
            {
                HttpCookie cookie = Controller.Request.Cookies[key];
                if (cookie != null)
                    return cookie.Value;
                else
                {
                    return "";
                }
            }
            public static string Get(HttpContextBase Context, string key)
            {
                HttpCookie cookie = Context.Request.Cookies[key];
                if (cookie != null)
                    return cookie.Value;
                else
                {
                    return "";
                }
            }
            public static List<KeyValuePair<string, string>> GetAll(HttpRequestBase Request, string startsWith)
            {
                var keys = Request.Cookies.AllKeys.Where(k => k.StartsWith(startsWith));
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                foreach (var key in keys)
                {
                    list.Add(new KeyValuePair<string, string>(key, Request.Cookies[key].Value));
                }
                return list;
            }
            public static List<KeyValuePair<string, string>> GetAll(HttpContextBase Context, string startsWith)
            {
                var keys = Context.Request.Cookies.AllKeys.Where(k => k.StartsWith(startsWith));
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                foreach (var key in keys)
                {
                    list.Add(new KeyValuePair<string, string>(key, Context.Request.Cookies[key].Value));
                }
                return list;
            }
            public static void Delete(Controller Controller, string key)
            {
                try
                {
                    HttpCookie cookie = Controller.Request.Cookies[key];
                    if (cookie != null)
                    {
                        Controller.Response.Cookies[key].Expires = DateTime.Now.AddDays(-1);
                    }
                }
                catch (Exception)
                {

                }
            }
            public static void Delete(HttpContextBase Context, string key)
            {
                try
                {
                    HttpCookie cookie = Context.Request.Cookies[key];
                    if (cookie != null)
                    {
                        Context.Response.Cookies[key].Expires = DateTime.Now.AddDays(-1);
                    }
                }
                catch (Exception)
                {

                }
            }
        }
        public static bool ToBool(this bool? b)
        {
            if (b == null)
                return false;
            bool result = false;
            bool.TryParse(b.ToString(), out result);
            return result;
        }
        public static decimal Ondalik(string str)
        {
            try
            {
                return Convert.ToDecimal(str.Replace('.', ','));
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static decimal Ondalik(decimal? dec)
        {
            try
            {
                return Convert.ToDecimal(dec);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static double Ondalik(double? dob)
        {
            try
            {
                return Convert.ToDouble(dob);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static double ToDouble(this object val)
        {
            try
            {
                return Convert.ToDouble(val);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static int Tamsayi(object obj)
        {
            try
            {
                return Convert.ToInt32(obj.ToString());
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static long Long(string longStr)
        {
            try
            {
                return Convert.ToInt64(longStr);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static string CalculateTotal(decimal? price, decimal? profit, int? vat)
        {
            try
            {
                return string.Format("{0:0.00}", ((Ondalik(price) + Ondalik(profit)) * (100 + (Tamsayi(vat))) / 100));
            }
            catch (Exception)
            {
                return "-";
            }
        }
        static int colorIndex = 0;
        public static Color getRandomColor()
        {
            try
            {
                KnownColor[] names = new KnownColor[] { KnownColor.DarkBlue, KnownColor.CadetBlue, KnownColor.BlueViolet, KnownColor.DeepSkyBlue, KnownColor.DodgerBlue, KnownColor.AliceBlue, KnownColor.CornflowerBlue, KnownColor.MediumPurple, KnownColor.Purple, KnownColor.Violet, KnownColor.DarkGray, KnownColor.DarkTurquoise, KnownColor.Turquoise };
                KnownColor randomColorName = names[colorIndex];
                colorIndex = colorIndex == names.Length - 1 ? 0 : ++colorIndex;
                return Color.FromKnownColor(randomColorName);
            }
            catch (Exception ex)
            {
                return Color.Black;
            }
        }

    }
}