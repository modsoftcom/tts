﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web
{
    public static class Helper
    {
        public static void OdalariDagit()
        {
            var bolumListesi = new List<bolumler>();
            using (BolumServisi Servis = new BolumServisi())
            {
                foreach (bolumler bolum in Servis.TumunuGetir())
                {
                    using (OdaServisi OdaServis = new OdaServisi())
                    {
                        if (bolum.odalar.Count == 0)
                            for (int i = 1; i <= bolum.oda_sayisi; i++)
                            {
                                odalar oda = new odalar();
                                oda.bolum_id = bolum.id;
                                oda.aktif_mi = true;
                                // TODO odada olacak hasta kapasitesini burada ayarlıyoruz!
                                oda.hasta_kapasitesi = 2;
                                oda.oda_no = ((bolum.id * 100) + i).ToString();
                                OdaServis.Ekle(oda);
                            }
                    }
                }
            }
            
        }

        public static List<SelectListItem> GetSelectListItems<T>(List<T> liste)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (T eleman in liste)
            {
                list.Add(new SelectListItem
                {
                    Text = eleman.ToString(),
                    Value = eleman.GetType().GetProperty("id").GetValue(eleman).ToString()
                });
            }
            return list;
        }
        public static string MakeActiveClass(this UrlHelper urlHelper, string controller, string action = null)
        {
            string result = null;

            string controllerName = urlHelper.RequestContext.RouteData.Values["controller"].ToString();
            string actionName = urlHelper.RequestContext.RouteData.Values["action"].ToString();
            if (action != null && controllerName.Equals(controller, StringComparison.OrdinalIgnoreCase) && actionName.Equals(action, StringComparison.OrdinalIgnoreCase))
                result = "active";
            else if (controllerName.Equals(controller, StringComparison.OrdinalIgnoreCase))
                result = "active";

            return result;
        }
        public static byte[] ToByteArray(this HttpPostedFileBase file)
        {
            byte[] imgData;
            using (var reader = new BinaryReader(file.InputStream))
            {
                imgData = reader.ReadBytes(file.ContentLength);
            }
            return imgData;
        }
        public static string ToEnglish(this string phrase)
        {
            phrase = phrase.Replace("ı", "i");
            phrase = phrase.Replace("ö", "o");
            phrase = phrase.Replace("ü", "u");
            phrase = phrase.Replace("ç", "c");
            phrase = phrase.Replace("ğ", "g");
            phrase = phrase.Replace("ş", "s");
            phrase = phrase.Replace("İ", "I");
            phrase = phrase.Replace("Ö", "O");
            phrase = phrase.Replace("Ü", "U");
            phrase = phrase.Replace("Ç", "C");
            phrase = phrase.Replace("Ğ", "G");
            phrase = phrase.Replace("Ş", "S");
            return phrase;
        }

        public static string Theme(string defaultTheme = "MaterialCompact")
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Theme"];
            if (cookie != null)
                return cookie.Value;
            else
            {
                return defaultTheme;
            }
        }

        public static string GetDisplayName<T>()
        {
            var displayName = typeof(T).GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }
        public static string GetDisplayName(this Type type)
        {
            var displayName = type.GetCustomAttributes(typeof(DisplayNameAttribute), true).SingleOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return "";
        }
        public static string GetDisplayName(this PropertyInfo property)
        {
            var displayName = property.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;

            return property.Name;
        }
        public static bool IsRequired(this PropertyInfo property)
        {
            object[] attrs = property.GetCustomAttributes(true);
            foreach (object attr in attrs)
            {
                RequiredAttribute reqAttr = attr as RequiredAttribute;
                if (reqAttr != null)
                    return true;
            }
            return false;
        }
        public static void Equalize(this object target, object source)
        {
            foreach (PropertyInfo prop in target.GetType().GetProperties().Where(p => !p.GetMethod.IsVirtual && p.Name != "created_date" && p.Name != "modified_date" && p.Name != "is_active" && p.Name != "details"))
            {
                var value = source.GetType().GetProperty(prop.Name).GetValue(source);
                target.GetType().GetProperty(prop.Name).SetValue(target, value);
            }
        }

        public static bool ValidateActionParameters(params int[] parameters)
        {
            foreach (int parameter in parameters)
            {
                if (parameter <= 0)
                    return false;
            }
            return true;
        }

        public static string InputType(this Type type)
        {
            switch (Type.GetTypeCode(type.GetType()))
            {
                case TypeCode.DateTime:
                    return "date";
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return "number";
                default:
                    return "text";
            }
        }
    }

}