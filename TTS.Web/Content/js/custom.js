﻿// When the user scrolls the page, execute myFunction 
window.onscroll = function () { StrickSetter() };

// Get the navbar
var navbar = document.getElementById("navigation-bar");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function StrickSetter() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}
function CalculateForex() {
    var value = $('#currency-value').val();
    var foreignCurrency = $("#forign-currency-rates option:selected").text();

    var buying = parseFloat($('#currency-rates').val().replace(",", "."));
    var foreingBuying = parseFloat($('#forign-currency-rates').val().replace(",", "."));
    $('#forex-result').html((value * buying / foreingBuying).toFixed(3) + ' ' + foreignCurrency);
}
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})