﻿using TTS.BLL;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class HesapController : Controller
    {
        public ActionResult Login(string errorMessage = null)
        {
            ViewBag.SisteminAdi = "Tedavi Takip Sistemi";
            ViewBag.ErrorMessage = Session["ErrorMessage"] != null ? Session["ErrorMessage"] : "";
            return View();
        }

        [HttpPost]
        public ActionResult Login(Kullanici model)
        {
            ViewBag.SisteminAdi = "Tedavi Takip Sistemi";
            using (HesapServisi hesapServisi = new HesapServisi())
            {
                Kullanici kullanici;
                kullanici = hesapServisi.DoktorGirisYap(model.tc_no, model.parola);

                if (kullanici == null)
                    kullanici = hesapServisi.HemsireGirisYap(model.tc_no, model.parola);

                if (kullanici == null)
                    kullanici = hesapServisi.GorevliGirisYap(model.tc_no, model.parola);

                if (kullanici != null)
                {
                    Session["Kullanici"] = kullanici;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.ErrorMessage = "Lütfen geçerli bir kullanıcı bilgisi giriniz.";
                    return View(model);
                }
            }
        }
        public ActionResult Profilim()
        {
            Session["Baslik"] = "Kullanıcı Bilgileri";
            Kullanici kullanici = Session["Kullanici"] as Kullanici;
            return View(kullanici);
        }
        [HttpPost]
        public ActionResult Profilim(int id, string old_password, string new_password)
        {
            using (HesapServisi hesapServisi = new HesapServisi())
            {
                Kullanici kullanici = hesapServisi.KullaniciAl(id);
                if (kullanici.parola == old_password)
                {

                    if (kullanici is doktorlar)
                    {
                        using (DoktorServisi Servis = new DoktorServisi())
                        {
                            doktorlar doktor = kullanici as doktorlar;
                            doktor.parola = new_password;
                            Servis.Duzenle(doktor);

                        }
                    }
                    if (kullanici is hemsireler)
                    {
                        using (HemsireServisi Servis = new HemsireServisi())
                        {
                            hemsireler hemsire = kullanici as hemsireler;
                            hemsire.parola = new_password;
                            Servis.Duzenle(hemsire);

                        }
                    }
                    if (kullanici is gorevliler)
                    {
                        using (GorevliServisi Servis = new GorevliServisi())
                        {
                            gorevliler gorevli = kullanici as gorevliler;
                            gorevli.parola = new_password;
                            Servis.Duzenle(gorevli);

                        }
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Lütfen geçerli bir parola giriniz.";
                }
                return View(kullanici);
            }
        }
        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }




    }
}