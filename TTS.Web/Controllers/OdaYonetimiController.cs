﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class OdaYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(odalar)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}

		public ActionResult Index()	
        {			
            Session["Baslik"] =  SinifAdi() + " Listesi";
            if (db.odalar.Count() == 0)
                Helper.OdalariDagit();
            var odalar = db.odalar.Include(o => o.bolumler).Include(o => o.hastalar);
			return View(odalar.Where(e=>e.aktif_mi).ToList());
        }
		public ActionResult Details(int? id)	
        {
            Session["Baslik"] =  SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			odalar odalar = db.odalar.Find(id);
            if (odalar == null)
            {
                return HttpNotFound();
            }
            return View(odalar);
        }
        public ActionResult Tahsis(int id, int bolum_id)
        {
            Session["Baslik"] =  SinifAdi() + " Tahsis Ekranı";
            Session["HastaId"] = id;
            return View(db.odalar.Where(o=>o.bolum_id == bolum_id));
        }
        public ActionResult TahsisEt(int id)
        {
            int hasta_id = (int)Session["HastaId"];
            using (HastaServisi Servis = new HastaServisi())
            {
                hastalar hasta = Servis.TekGetir(hasta_id);
                hasta.oda_id = id;
                Servis.Duzenle(hasta);
            }
            odalar oda = db.odalar.Find(id);

            using (BLL.HastaIslemServisi Servis = new BLL.HastaIslemServisi())
            {
                hastanin_islemleri islem = new hastanin_islemleri();
                islem.aciklama = string.Format("Hastanın yatış işlemi gerçekleştirildi. Hasta, {0} kliniğindeki {1} numaralı odaya yatırıldı.", oda.bolumler.adi, oda.oda_no);
                islem.aktif_mi = true;
                islem.hasta_id = (int)Session["HastaId"];
                islem.gorevli_id = OAnkiKullanici.id;
                islem.islem_id = 5;
                islem.islem_zamani = DateTime.Now;
                Servis.Ekle(islem);
            }
            return RedirectToAction("Details", "HastaYonetimi", new { id = (int)Session["HastaId"] }); ;
        }
        public ActionResult Bosalt(int id)
        {
            using (BLL.HastaServisi Servis = new BLL.HastaServisi())
            {
                hastalar hasta = Servis.TekGetir(id);
                hasta.oda_id = null;
                Servis.Duzenle(hasta);
            }
            using (BLL.HastaIslemServisi Servis = new BLL.HastaIslemServisi())
            {
                hastanin_islemleri islem = new hastanin_islemleri();
                islem.aciklama = "Hastanın çıkışı yapıldı.";
                islem.aktif_mi = true;
                islem.hasta_id = id;
                islem.gorevli_id = OAnkiKullanici.id;
                islem.islem_id = 8;
                islem.islem_zamani = DateTime.Now;
                Servis.Ekle(islem);
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult BaskaHastayeGonder()
        {
            using (BLL.HastaIslemServisi Servis = new BLL.HastaIslemServisi())
            {
                hastanin_islemleri islem = new hastanin_islemleri();
                islem.aciklama = "Bölümde boş oda olmadığından hasta, başka hastaneye sevk edildi.";
                islem.aktif_mi = true;
                islem.hasta_id = (int)Session["HastaId"];
                islem.gorevli_id = OAnkiKullanici.id;
                islem.islem_id = 6;
                islem.islem_zamani = DateTime.Now;
                Servis.Ekle(islem);
            }
            return RedirectToAction("Details", "HastaYonetimi", new { id = (int)Session["HastaId"] }); 
        }
        public ActionResult Create()
        {
            Session["Baslik"] =  SinifAdi() + " Oluşturma Ekranı";
            ViewBag.bolum_id = db.bolumler.Select(b => new SelectListItem
            {
                Text = b.adi,
                Value = b.id.ToString()
            });
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,bolum_id,oda_no,hasta_kapasitesi,aktif_mi")] odalar odalar)

        {
			odalar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.odalar.Add(odalar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.bolum_id = db.bolumler.Select(b => new SelectListItem
            {
                Text = b.adi,
                Value = b.id.ToString()
            });
            return View(odalar);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odalar odalar = db.odalar.Find(id);

            if (odalar == null)
            {
                return HttpNotFound();
            }
            ViewBag.bolum_id = Helper.GetSelectListItems(db.bolumler.ToList());
            return View(odalar);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,bolum_id,oda_no,hasta_kapasitesi,aktif_mi")] odalar odalar)

        {
			odalar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(odalar).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.bolum_id = Helper.GetSelectListItems(db.bolumler.ToList());
            return View(odalar);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            odalar odalar = db.odalar.Find(id);

            if (odalar == null)
            {
                return HttpNotFound();
            }
            return View(odalar);
        }
        public ActionResult DeleteConfirmed(int id)

        {
            odalar odalar = db.odalar.Find(id);            
            db.Entry(odalar).State = EntityState.Modified;
            odalar.aktif_mi = false;            
			db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
