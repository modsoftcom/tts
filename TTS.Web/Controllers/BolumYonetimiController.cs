﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class BolumYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(bolumler)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}

		public ActionResult Index()	
        {			
            Session["Baslik"] =  SinifAdi() + " Listesi";
			return View(db.bolumler.Where(e=>e.aktif_mi).ToList());
        }
		public ActionResult Details(int? id)	
        {
            Session["Baslik"] =  SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			bolumler bolumler = db.bolumler.Find(id);
            if (bolumler == null)
            {
                return HttpNotFound();
            }
            return View(bolumler);
        }
        public ActionResult Create()
        {
            Session["Baslik"] =  SinifAdi() + " Oluşturma Ekranı";
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,oda_sayisi,aktif_mi")] bolumler bolumler)

        {
			bolumler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.bolumler.Add(bolumler);
                db.SaveChanges();
                Helper.OdalariDagit();
                return RedirectToAction("Index");
            }

            return View(bolumler);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            bolumler bolumler = db.bolumler.Find(id);

            if (bolumler == null)
            {
                return HttpNotFound();
            }
            return View(bolumler);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,oda_sayisi,aktif_mi")] bolumler bolumler)

        {
			bolumler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(bolumler).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(bolumler);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            bolumler bolumler = db.bolumler.Find(id);

            if (bolumler == null)
            {
                return HttpNotFound();
            }
            return View(bolumler);
        }
        public ActionResult DeleteConfirmed(int id)

        {
            bolumler bolumler = db.bolumler.Find(id);            
            db.Entry(bolumler).State = EntityState.Modified;
            bolumler.aktif_mi = false;            
			db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
