﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class DoktorYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

        private string SinifAdi()
        {
            var displayName = typeof(doktorlar)
                .GetCustomAttributes(typeof(DisplayNameAttribute), true)
                .FirstOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;
            else
                return "Kayıt";
        }

        public ActionResult Index()
        {
            Session["Baslik"] = SinifAdi() + " Listesi";
            var doktorlar = db.doktorlar.Include(d => d.bolumler);
            return View(doktorlar.Where(e => e.aktif_mi).ToList());
        }
        public ActionResult Details(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            doktorlar doktorlar = db.doktorlar.Find(id);
            if (doktorlar == null)
            {
                return HttpNotFound();
            }
            return View(doktorlar);
        }
        public ActionResult Create()
        {
            Session["Baslik"] = SinifAdi() + " Oluşturma Ekranı";
            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,soyadi,bolum_id,tc_no,parola,aktif_mi")] doktorlar doktorlar)

        {
            using (HesapServisi Servis = new HesapServisi())
            {
                if (!Servis.KullaniciAdiUygunMu(doktorlar.tc_no))
                {
                    ViewBag.TCUygunDegil = "TC Kimlik No mevcut!";
                    ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
                    return View(doktorlar);
                }
            }
            doktorlar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.doktorlar.Add(doktorlar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(doktorlar);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            doktorlar doktorlar = db.doktorlar.Find(id);

            if (doktorlar == null)
            {
                return HttpNotFound();
            }
            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            Session["TC"] = doktorlar.tc_no;
            return View(doktorlar);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,soyadi,bolum_id,tc_no,parola,aktif_mi")] doktorlar doktorlar)

        {
            if (Session["TC"].ToString() != doktorlar.tc_no)
                using (HesapServisi Servis = new HesapServisi())
                {
                    if (!Servis.KullaniciAdiUygunMu(doktorlar.tc_no))
                    {
                        ViewBag.TCUygunDegil = "TC Kimlik No mevcut!";
                        ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
                        return View(doktorlar);
                    }
                }
            doktorlar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(doktorlar).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(doktorlar);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            doktorlar doktorlar = db.doktorlar.Find(id);

            if (doktorlar == null)
            {
                return HttpNotFound();
            }
            return View(doktorlar);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            doktorlar doktorlar = db.doktorlar.Find(id);
            db.Entry(doktorlar).State = EntityState.Modified;
            doktorlar.aktif_mi = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
