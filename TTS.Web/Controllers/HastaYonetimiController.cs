﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class HastaYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(hastalar)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}

		public ActionResult Index()	
        {			
            Session["Baslik"] =  SinifAdi() + " Listesi";
            var hastalar = db.hastalar.Include(h => h.hemsireler);
            return View(hastalar.Where(e=>e.aktif_mi).ToList());
        }
		public ActionResult Details(int? id)	
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			hastalar hastalar = db.hastalar.Find(id);
            if (hastalar == null)
            {
                return HttpNotFound();
            }
            Session["Baslik"] = string.Format("{0} - {1} {2}",hastalar.tc_no,hastalar.adi,hastalar.soyadi);
            return View(hastalar);
        }
        public ActionResult Create()
        {
            Session["Baslik"] =  SinifAdi() + " Oluşturma Ekranı";
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h=>!h.bas_hemsire_mi&&h.aktif_mi).ToList());
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,soyadi,tc_no,hemsire_id,aktif_mi")] hastalar hastalar,string sikayeti = "")

        {
			hastalar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.hastalar.Add(hastalar);
                db.SaveChanges();

                using (BLL.HastaIslemServisi Servis = new BLL.HastaIslemServisi())
                {
                    hastanin_islemleri islem = new hastanin_islemleri();
                    islem.hasta_id = hastalar.id;
                    islem.aktif_mi = true;
                    islem.gorevli_id = base.OAnkiKullanici.id;
                    islem.islem_id = 1;
                    islem.islem_zamani = DateTime.Now;
                    islem.aciklama = sikayeti;
                    Servis.Ekle(islem);
                }
                return RedirectToAction("Index");
            }

            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h => !h.bas_hemsire_mi && h.aktif_mi).ToList());
            return View(hastalar);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hastalar hastalar = db.hastalar.Find(id);

            if (hastalar == null)
            {
                return HttpNotFound();
            }
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h => !h.bas_hemsire_mi && h.aktif_mi).ToList());
            return View(hastalar);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,soyadi,tc_no,hemsire_id,aktif_mi")] hastalar hastalar)

        {
			hastalar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(hastalar).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h => !h.bas_hemsire_mi && h.aktif_mi).ToList());
            return View(hastalar);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hastalar hastalar = db.hastalar.Find(id);

            if (hastalar == null)
            {
                return HttpNotFound();
            }
            return View(hastalar);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            hastalar hastalar = db.hastalar.Find(id);            
            db.Entry(hastalar).State = EntityState.Modified;
            hastalar.aktif_mi = false;            
			db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
