﻿using TTS.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class HomeController : TemelController
    {
        public ActionResult Index()
        {
            Session["Baslik"] = "Giriş Sayfası";
            return View();
        }

        public ActionResult ErrorPage()
        {
            Session["Baslik"] = "Sayfa Bulunamadı";
            return View();
        }
        public ActionResult InternalServerError()
        {
            Session["Baslik"] = "Sunucu Hatası";
            return View();
        }
    }
}
