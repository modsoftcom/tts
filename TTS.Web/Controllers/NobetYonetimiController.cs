﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class NobetYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(nobetler)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}
        public ActionResult Takvim()
        {
            Session["Baslik"] = SinifAdi() + " Takvimi";
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h => h.id != OAnkiKullanici.id).ToList());
            return View();
        }
        public JsonResult Liste(DateTime start, DateTime end)
        {
            List<TakvimModeli> liste = new List<TakvimModeli>();
            foreach (var nobet in db.nobetler.Where(n=>n.aktif_mi).Include(n => n.hemsireler).Include(n => n.hemsireler.bolumler))
            {
                liste.Add(new TakvimModeli
                {
                    id = nobet.id,
                    title = string.Format("{0} {1}\n{2}", nobet.hemsireler.adi, nobet.hemsireler.soyadi, nobet.hemsireler.bolumler.adi),
                    start = nobet.tarih.ToString("yyyy-MM-dd"),
                    end = nobet.tarih.ToString("yyyy-MM-dd")
                });
            }
            return Json(liste, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Degistir(int id,DateTime start)
        {
            nobetler nobet = db.nobetler.Find(id);
            nobet.tarih = start;
            db.Entry(nobet).State = EntityState.Modified;
            db.SaveChanges();
            string message = NobetiKontrolEt(nobet);
            return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index()
        {
            return RedirectToAction("Takvim");
        }
		public ActionResult Details(int? id)	
        {
            Session["Baslik"] =  SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			nobetler nobetler = db.nobetler.Find(id);
            if (nobetler == null)
            {
                return HttpNotFound();
            }
            return View(nobetler);
        }
        public ActionResult Create()
        {
            Session["Baslik"] =  SinifAdi() + " Oluşturma Ekranı";
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h=>h.id != OAnkiKullanici.id).ToList());
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Create(string hemsire_id, string tarih)

        {
            nobetler nobetler = new nobetler();
            nobetler.hemsire_id = Convert.ToInt32(hemsire_id);
            nobetler.tarih = DateTime.ParseExact(tarih, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            nobetler.aktif_mi = true;
            db.nobetler.Add(nobetler);
            db.SaveChanges();

            string message = NobetiKontrolEt(nobetler);
            return Json(new { Message = message }, JsonRequestBehavior.AllowGet);
        }

        private string NobetiKontrolEt(nobetler nobetler)
        {
            string message = "";
            foreach (nobetler nobet in db.nobetler.Where(n => n.aktif_mi && n.hemsire_id == nobetler.hemsire_id))
            {

                DateTime yarin = nobet.tarih.AddDays(1);
                if (nobetler.tarih.Ticks == yarin.Ticks)
                {
                    message = "DİKKAT! Bu hemşire bir önceki gün de nöbetçidir.\n";
                }

                DateTime dun = nobet.tarih.AddDays(-1);
                if (nobetler.tarih.Ticks == dun.Ticks)
                {
                    message += "DİKKAT! Bu hemşire bir sonraki gün de nöbetçidir.";
                }
            }

            return message;
        }

        public ActionResult Edit(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            nobetler nobetler = db.nobetler.Find(id);

            if (nobetler == null)
            {
                return HttpNotFound();
            }
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h => h.id != OAnkiKullanici.id).ToList());
            return View(nobetler);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,hemsire_id,tarih,aktif_mi")] nobetler nobetler)

        {
			nobetler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(nobetler).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Takvim");
            }
            ViewBag.hemsire_id = Helper.GetSelectListItems(db.hemsireler.Where(h => h.id != OAnkiKullanici.id).ToList());
            return View(nobetler);
        }

        public ActionResult Delete(int id)
        {
            nobetler nobetler = db.nobetler.Find(id);
            db.Entry(nobetler).State = EntityState.Modified;
            nobetler.aktif_mi = false;
            db.SaveChanges();
            return RedirectToAction("Takvim");
        }

        public ActionResult DeleteConfirmed(int id)

        {
            nobetler nobetler = db.nobetler.Find(id);            
            db.Entry(nobetler).State = EntityState.Modified;
            nobetler.aktif_mi = false;            
			db.SaveChanges();
            return RedirectToAction("Takvim");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
