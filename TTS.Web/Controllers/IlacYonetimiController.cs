﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class IlacYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(ilaclar)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}

		public ActionResult Index()	
        {			
            Session["Baslik"] =  SinifAdi() + " Listesi";
			return View(db.ilaclar.Where(e=>e.aktif_mi).ToList());
        }
		public ActionResult Details(int? id)	
        {
            Session["Baslik"] =  SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			ilaclar ilaclar = db.ilaclar.Find(id);
            if (ilaclar == null)
            {
                return HttpNotFound();
            }
            return View(ilaclar);
        }
        public ActionResult Create()
        {
            Session["Baslik"] =  SinifAdi() + " Oluşturma Ekranı";
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,doz_sayisi,birim,aktif_mi")] ilaclar ilaclar)

        {
			ilaclar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.ilaclar.Add(ilaclar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ilaclar);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilaclar ilaclar = db.ilaclar.Find(id);

            if (ilaclar == null)
            {
                return HttpNotFound();
            }
            return View(ilaclar);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,doz_sayisi,birim,aktif_mi")] ilaclar ilaclar)

        {
			ilaclar.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(ilaclar).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(ilaclar);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ilaclar ilaclar = db.ilaclar.Find(id);

            if (ilaclar == null)
            {
                return HttpNotFound();
            }
            return View(ilaclar);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            ilaclar ilaclar = db.ilaclar.Find(id);            
            db.Entry(ilaclar).State = EntityState.Modified;
            ilaclar.aktif_mi = false;            
			db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
