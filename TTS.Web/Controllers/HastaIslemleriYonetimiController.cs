﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class HastaIslemleriYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

        private string SinifAdi()
        {
            var displayName = typeof(hastanin_islemleri)
                .GetCustomAttributes(typeof(DisplayNameAttribute), true)
                .FirstOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;
            else
                return "Kayıt";
        }

        public ActionResult Index()
        {
            Session["Baslik"] = SinifAdi() + " Listesi";
            var hastanin_islemleri = db.hastanin_islemleri.Include(h => h.doktorlar).Include(h => h.gorevliler).Include(h => h.hastalar).Include(h => h.hemsireler).Include(h => h.islemler);
            return View(hastanin_islemleri.Where(e => e.aktif_mi).ToList());
        }

        public ActionResult IlacVer(int id, string callbackUrl)
        {
            Session["Baslik"] = SinifAdi() + " Detay Ekranı";
            hastanin_ilaclari hasta_ilaci = db.hastanin_ilaclari.Find(id);
            using (HastaIslemServisi Servis = new HastaIslemServisi())
            {
                hastanin_islemleri hasta_islemi = new hastanin_islemleri();
                hasta_islemi.aciklama = string.Format("Hastaya {0} {1} {2} ilacı verildi.", hasta_ilaci.doz_sayisi, hasta_ilaci.ilaclar.birim, hasta_ilaci.ilaclar.adi);
                Kullanici kullanici = Session["Kullanici"] as Kullanici;
                hasta_islemi.hemsire_id = kullanici.id;
                hasta_islemi.aktif_mi = true;
                hasta_islemi.hasta_id = hasta_ilaci.hasta_id;
                hasta_islemi.islem_id = 3;
                hasta_islemi.islem_zamani = DateTime.Now;
                Servis.Ekle(hasta_islemi);
            }
            return Redirect(callbackUrl);
        }
        public ActionResult Create(int id, string callbackUrl)
        {
            Session["Baslik"] = SinifAdi() + " Oluşturma Ekranı";
            Session["Hasta"] = new HastaServisi().TekGetir(id);
            Session["GelinenAdres"] = callbackUrl;
            Kullanici kullanici = Session["Kullanici"] as Kullanici;
            var islemler = db.islemler.AsEnumerable();
            if (kullanici is doktorlar)
                islemler = islemler.Where(i => i.doktor_yapabilir_mi);
            else if (kullanici is hemsireler)
                islemler = islemler.Where(i => i.hemsire_yapabilir_mi);
            else if(kullanici is gorevliler && kullanici.tc_no != "admin")
                islemler = islemler.Where(i => i.gorevli_yapabilir_mi);

            ViewBag.islem_id = islemler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,hasta_id,islem_id,doktor_id,gorevli_id,hemsire_id,aciklama,islem_zamani,aktif_mi")] hastanin_islemleri hastanin_islemleri)

        {
            hastanin_islemleri.aktif_mi = true;
            hastalar hasta = Session["Hasta"] as hastalar;
            Kullanici kullanici = Session["Kullanici"] as Kullanici;
            var islemler = db.islemler.AsEnumerable();
            if (kullanici is doktorlar)
            {
                hastanin_islemleri.doktor_id = kullanici.id;
                islemler = islemler.Where(i => i.doktor_yapabilir_mi);
            }
            else if (kullanici is hemsireler)
            {
                hastanin_islemleri.hemsire_id = kullanici.id;
                islemler = islemler.Where(i => i.hemsire_yapabilir_mi);
            }
            else
            {
                hastanin_islemleri.gorevli_id = kullanici.id;
                islemler = islemler.Where(i => i.gorevli_yapabilir_mi);
            }
            hastanin_islemleri.hasta_id = hasta.id;
            hastanin_islemleri.islem_zamani = DateTime.Now;
            db.hastanin_islemleri.Add(hastanin_islemleri);
            db.SaveChanges();
            return Redirect(Session["GelinenAdres"].ToString());
        }
        public ActionResult Edit(int? id, string callbackUrl)
        {
            Session["Baslik"] = SinifAdi() + " Düzenleme Ekranı";
            Session["GelinenAdres"] = callbackUrl;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hastanin_islemleri hastanin_islemleri = db.hastanin_islemleri.Find(id);

            if (hastanin_islemleri == null)
            {
                return HttpNotFound();
            }

            Kullanici kullanici = Session["Kullanici"] as Kullanici;
            var islemler = db.islemler.AsEnumerable();
            if (kullanici is doktorlar)
                islemler = islemler.Where(i => i.doktor_yapabilir_mi);
            else if (kullanici is hemsireler)
                islemler = islemler.Where(i => i.hemsire_yapabilir_mi);
            else
                islemler = islemler.Where(i => i.gorevli_yapabilir_mi);

            ViewBag.islem_id = islemler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(hastanin_islemleri);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,hasta_id,islem_id,doktor_id,gorevli_id,hemsire_id,aciklama,islem_zamani,aktif_mi")] hastanin_islemleri hastanin_islemleri)

        {
            hastanin_islemleri.aktif_mi = true;
            Kullanici kullanici = Session["Kullanici"] as Kullanici;
            var islemler = db.islemler.AsEnumerable();
            if (kullanici is doktorlar)
            {
                islemler = islemler.Where(i => i.doktor_yapabilir_mi);
            }
            else if (kullanici is hemsireler)
            {
                islemler = islemler.Where(i => i.hemsire_yapabilir_mi);
            }
            else
            {
                islemler = islemler.Where(i => i.gorevli_yapabilir_mi);
            }
            if (ModelState.IsValid)
            {
                db.Entry(hastanin_islemleri).State = EntityState.Modified;
                db.SaveChanges();
                if (string.IsNullOrEmpty(Session["GelinenAdres"].ToString()))
                    return RedirectToAction("Index");
                else
                    return Redirect(Session["GelinenAdres"].ToString());
            }
            ViewBag.islem_id = islemler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(hastanin_islemleri);
        }

        public ActionResult Delete(int? id, string callbackUrl)
        {
            Session["Baslik"] = SinifAdi() + " Silme Ekranı";
            Session["GelinenAdres"] = callbackUrl;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hastanin_islemleri hastanin_islemleri = db.hastanin_islemleri.Find(id);

            if (hastanin_islemleri == null)
            {
                return HttpNotFound();
            }
            return View(hastanin_islemleri);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            hastanin_islemleri hastanin_islemleri = db.hastanin_islemleri.Find(id);
            db.Entry(hastanin_islemleri).State = EntityState.Modified;
            hastanin_islemleri.aktif_mi = false;
            db.SaveChanges();
            return Redirect(Session["GelinenAdres"].ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
