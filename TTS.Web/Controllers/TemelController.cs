﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class TemelController : Controller
    {
        public Kullanici OAnkiKullanici { get; set; }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Session.Timeout = 300;
            OAnkiKullanici = Session["Kullanici"] as Kullanici;

            if (OAnkiKullanici == null)
            {
                filterContext.Result = Giris();
                return;
            }
            base.OnActionExecuting(filterContext);
        }
        public RedirectToRouteResult Giris()
        {
            return RedirectToAction("Login", "Hesap");
        }

    }
}