﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class IslemYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(islemler)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}

		public ActionResult Index()	
        {			
            Session["Baslik"] =  SinifAdi() + " Listesi";
			return View(db.islemler.Where(e=>e.aktif_mi).ToList());
        }
		public ActionResult Details(int? id)	
        {
            Session["Baslik"] =  SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			islemler islemler = db.islemler.Find(id);
            if (islemler == null)
            {
                return HttpNotFound();
            }
            return View(islemler);
        }
        public ActionResult Create()
        {
            Session["Baslik"] =  SinifAdi() + " Oluşturma Ekranı";
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,doktor_yapabilir_mi,hemsire_yapabilir_mi,gorevli_yapabilir_mi,aktif_mi")] islemler islemler)

        {
			islemler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.islemler.Add(islemler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(islemler);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            islemler islemler = db.islemler.Find(id);

            if (islemler == null)
            {
                return HttpNotFound();
            }
            return View(islemler);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,doktor_yapabilir_mi,hemsire_yapabilir_mi,gorevli_yapabilir_mi,aktif_mi")] islemler islemler)

        {
			islemler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(islemler).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(islemler);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] =  SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            islemler islemler = db.islemler.Find(id);

            if (islemler == null)
            {
                return HttpNotFound();
            }
            return View(islemler);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            islemler islemler = db.islemler.Find(id);            
            db.Entry(islemler).State = EntityState.Modified;
            islemler.aktif_mi = false;            
			db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
