﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class HemsireYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

        private string SinifAdi()
        {
            var displayName = typeof(hemsireler)
                .GetCustomAttributes(typeof(DisplayNameAttribute), true)
                .FirstOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;
            else
                return "Kayıt";
        }

        public ActionResult Index()
        {
            Session["Baslik"] = SinifAdi() + " Listesi";
            var hemsireler = db.hemsireler.Include(h => h.bolumler).Include(h => h.doktorlar).Where(e => e.aktif_mi).ToList();
            return View(hemsireler);
        }
        public ActionResult Details(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hemsireler hemsireler = db.hemsireler.Find(id);
            if (hemsireler == null)
            {
                return HttpNotFound();
            }
            return View(hemsireler);
        }
        public ActionResult Create()
        {
            Session["Baslik"] = SinifAdi() + " Oluşturma Ekranı";
            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            
            ViewBag.doktor_id = Helper.GetSelectListItems(db.doktorlar.ToList());
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,soyadi,bolum_id,doktor_id,tc_no,parola,bas_hemsire_mi,aktif_mi")] hemsireler hemsireler)

        {
            using (HesapServisi Servis = new HesapServisi())
            {
                if (!Servis.KullaniciAdiUygunMu(hemsireler.tc_no))
                {
                    ViewBag.TCUygunDegil = "TC Kimlik No mevcut!";
                    ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
                    ViewBag.doktor_id = Helper.GetSelectListItems(db.doktorlar.ToList());
                    return View(hemsireler);
                }
            }
            hemsireler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.hemsireler.Add(hemsireler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            ViewBag.doktor_id = Helper.GetSelectListItems(db.doktorlar.ToList());
            return View(hemsireler);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hemsireler hemsireler = db.hemsireler.Find(id);

            if (hemsireler == null)
            {
                return HttpNotFound();
            }
            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            ViewBag.doktor_id = Helper.GetSelectListItems(db.doktorlar.ToList());
            Session["TC"] = hemsireler.tc_no;
            return View(hemsireler);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,soyadi,bolum_id,doktor_id,tc_no,parola,bas_hemsire_mi,aktif_mi")] hemsireler hemsireler)

        {
            if (Session["TC"].ToString() != hemsireler.tc_no)
                using (HesapServisi Servis = new HesapServisi())
                {
                    if (!Servis.KullaniciAdiUygunMu(hemsireler.tc_no))
                    {
                        ViewBag.TCUygunDegil = "TC Kimlik No mevcut!";
                        ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
                        ViewBag.doktor_id = Helper.GetSelectListItems(db.doktorlar.ToList());
                        return View(hemsireler);
                    }
                }
            hemsireler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(hemsireler).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.bolum_id = db.bolumler.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            ViewBag.doktor_id = Helper.GetSelectListItems(db.doktorlar.ToList());
            return View(hemsireler);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hemsireler hemsireler = db.hemsireler.Find(id);

            if (hemsireler == null)
            {
                return HttpNotFound();
            }
            return View(hemsireler);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            hemsireler hemsireler = db.hemsireler.Find(id);
            db.Entry(hemsireler).State = EntityState.Modified;
            hemsireler.aktif_mi = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
