﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class GorevliYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

        private string SinifAdi()
        {
            var displayName = typeof(gorevliler)
                .GetCustomAttributes(typeof(DisplayNameAttribute), true)
                .FirstOrDefault() as DisplayNameAttribute;

            if (displayName != null)
                return displayName.DisplayName;
            else
                return "Kayıt";
        }

        public ActionResult Index()
        {
            Session["Baslik"] = SinifAdi() + " Listesi";
            return View(db.gorevliler.Where(e => e.aktif_mi).ToList());
        }
        public ActionResult Details(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            gorevliler gorevliler = db.gorevliler.Find(id);
            if (gorevliler == null)
            {
                return HttpNotFound();
            }
            return View(gorevliler);
        }
        public ActionResult Create()
        {
            Session["Baslik"] = SinifAdi() + " Oluşturma Ekranı";
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,adi,soyadi,tc_no,parola,aktif_mi")] gorevliler gorevliler)

        {
            using (HesapServisi Servis = new HesapServisi())
            {
                if (!Servis.KullaniciAdiUygunMu(gorevliler.tc_no))
                {
                    ViewBag.TCUygunDegil = "TC Kimlik No mevcut!";
                    return View(gorevliler);
                }
            }
            gorevliler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.gorevliler.Add(gorevliler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gorevliler);
        }
        public ActionResult Edit(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Düzenleme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            gorevliler gorevliler = db.gorevliler.Find(id);

            if (gorevliler == null)
            {
                return HttpNotFound();
            }
            Session["TC"] = gorevliler.tc_no;
            return View(gorevliler);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,adi,soyadi,tc_no,parola,aktif_mi")] gorevliler gorevliler)

        {
            if (Session["TC"].ToString() != gorevliler.tc_no)
                using (HesapServisi Servis = new HesapServisi())
                {
                    if (!Servis.KullaniciAdiUygunMu(gorevliler.tc_no))
                    {
                        ViewBag.TCUygunDegil = "TC Kimlik No mevcut!";
                        return View(gorevliler);
                    }
                }
            gorevliler.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(gorevliler).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(gorevliler);
        }

        public ActionResult Delete(int? id)
        {
            Session["Baslik"] = SinifAdi() + " Silme Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            gorevliler gorevliler = db.gorevliler.Find(id);

            if (gorevliler == null)
            {
                return HttpNotFound();
            }
            return View(gorevliler);
        }

        public ActionResult DeleteConfirmed(int id)

        {
            gorevliler gorevliler = db.gorevliler.Find(id);
            db.Entry(gorevliler).State = EntityState.Modified;
            gorevliler.aktif_mi = false;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
