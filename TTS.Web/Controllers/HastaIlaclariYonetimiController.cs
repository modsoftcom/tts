﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TTS.BLL;
using TTS.DAL;

namespace TTS.Web.Controllers
{
    public class HastaIlaclariYonetimiController : TemelController
    {
        private TTSEntities db = new TTSEntities();

		private string SinifAdi(){
			var displayName = typeof(hastanin_ilaclari)
				.GetCustomAttributes(typeof(DisplayNameAttribute), true)
				.FirstOrDefault() as DisplayNameAttribute;

			if (displayName != null)
				return displayName.DisplayName;
			else
				return "Kayıt";
		}

		public ActionResult Index()	
        {			
            Session["Baslik"] =  SinifAdi() + " Listesi";
            var hastanin_ilaclari = db.hastanin_ilaclari.Include(h => h.doktorlar).Include(h => h.hastalar).Include(h => h.ilaclar);
			return View(hastanin_ilaclari.Where(e=>e.aktif_mi).ToList());
        }
		public ActionResult Details(int? id)	
        {
            Session["Baslik"] =  SinifAdi() + " Detay Ekranı";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			hastanin_ilaclari hastanin_ilaclari = db.hastanin_ilaclari.Find(id);
            if (hastanin_ilaclari == null)
            {
                return HttpNotFound();
            }
            return View(hastanin_ilaclari);
        }
        public ActionResult Create(int id, string callbackUrl)
        {
            Session["Baslik"] = SinifAdi() + " Oluşturma Ekranı";
            Kullanici kullanici = Session["Kullanici"] as Kullanici;
            Session["HastaId"] = id;
            Session["GelinenAdres"] = callbackUrl;
            Session["DoktorId"] = kullanici.id;

            ViewBag.ilac_id = db.ilaclar.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View();
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,hasta_id,ilac_id,doz_sayisi,gunde_kac_kez,doktor_id,aktif_mi")] hastanin_ilaclari hastanin_ilaclari)

        {
            hastanin_ilaclari.hasta_id = (int)Session["HastaId"];
            hastanin_ilaclari.doktor_id = (int)Session["DoktorId"];
            hastanin_ilaclari.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.hastanin_ilaclari.Add(hastanin_ilaclari);
                db.SaveChanges();
                return Redirect(Session["GelinenAdres"].ToString());
            }
            ViewBag.ilac_id = db.ilaclar.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(hastanin_ilaclari);
        }
        public ActionResult Edit(int? id, string callbackUrl)
        {
            Session["Baslik"] =  SinifAdi() + " Düzenleme Ekranı";
            Session["GelinenAdres"] = callbackUrl;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hastanin_ilaclari hastanin_ilaclari = db.hastanin_ilaclari.Find(id);

            if (hastanin_ilaclari == null)
            {
                return HttpNotFound();
            }
            ViewBag.ilac_id = db.ilaclar.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(hastanin_ilaclari);
        }
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,hasta_id,ilac_id,doz_sayisi,gunde_kac_kez,doktor_id,aktif_mi")] hastanin_ilaclari hastanin_ilaclari)

        {
			hastanin_ilaclari.aktif_mi = true;
            if (ModelState.IsValid)
            {
                db.Entry(hastanin_ilaclari).State = EntityState.Modified;
                db.SaveChanges();

                return Redirect(Session["GelinenAdres"].ToString());
            }
            ViewBag.ilac_id =db.ilaclar.Select(s => new SelectListItem() { Text = s.adi, Value = s.id.ToString() }).ToList();
            return View(hastanin_ilaclari);
        }

        public ActionResult Delete(int? id, string callbackUrl)
        {
            Session["Baslik"] =  SinifAdi() + " Silme Ekranı";
            Session["GelinenAdres"] = callbackUrl;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hastanin_ilaclari hastanin_ilaclari = db.hastanin_ilaclari.Find(id);

            if (hastanin_ilaclari == null)
            {
                return HttpNotFound();
            }
            return View(hastanin_ilaclari);
        }
        public ActionResult DeleteConfirmed(int id)

        {
            hastanin_ilaclari hastanin_ilaclari = db.hastanin_ilaclari.Find(id);            
            db.Entry(hastanin_ilaclari).State = EntityState.Modified;
            hastanin_ilaclari.aktif_mi = false;            
			db.SaveChanges();
            return Redirect(Session["GelinenAdres"].ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
