﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTS.Web.Models
{
    public class DataModel
    {
        public Type Type;
        public Action Action { get; set; }
        public string Title { get; set; }
        public DataModel()
        {

        }
        public DataModel(Type type, Action action)
        {
            Type = type;
            Action = action;
            switch (Action)
            {
                case Action.Create:
                    Title= type.GetDisplayName() + " Oluşturma";
                    break;
                case Action.Update:
                    Title = type.GetDisplayName() + " Düzenleme";
                    break;
                default:
                    break;
            }
        }
    }
    public enum Action{
        Create,
        Update
    }
}