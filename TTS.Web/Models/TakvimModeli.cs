﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTS.Web
{
    public class TakvimModeli
    {
        public int id { get; set; }
        public string title { get; set; }
        public object start { get; set; }
        public string end { get; set; }
    }
}