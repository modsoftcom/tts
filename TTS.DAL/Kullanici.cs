﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTS.DAL
{
    public class Kullanici
    {
        public int id { get; set; }
        [Display(Name="TC Kimlik No")]
        public string tc_no { get; set; }
        [Display(Name = "Parola")]
        public string parola { get; set; }

        [Display(Name = "Adı")]
        public string adi { get; set; }
        [Display(Name = "Soyadı")]
        public string soyadi { get; set; }
    }
}
