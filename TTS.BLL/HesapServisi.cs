﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class HesapServisi:IDisposable
    {
        TTSEntities Context;
        public HesapServisi()
        {
            Context = new TTSEntities();
        }

        #region Giriş Metodları


        public doktorlar DoktorGirisYap(string kimlik_no, string parola)
        {
            doktorlar doktor = Context.doktorlar.FirstOrDefault(x => x.tc_no == kimlik_no && x.parola == parola);
            return doktor;
        }

        public hemsireler HemsireGirisYap(string kimlik_no, string parola)
        {
            hemsireler hemsire = Context.hemsireler.FirstOrDefault(x => x.tc_no == kimlik_no && x.parola == parola);
            return hemsire;
        }

        public gorevliler GorevliGirisYap(string kimlik_no, string parola)
        {
            gorevliler gorevli = Context.gorevliler.FirstOrDefault(x => x.tc_no == kimlik_no && x.parola == parola);
            return gorevli;
        } 
        #endregion

        public bool KullaniciAdiUygunMu(string tc)
        {
            bool result = true;

            using (DoktorServisi servis = new DoktorServisi())
            {
                if (servis.TekGetir(d => d.tc_no == tc) != null)
                    return false;
            }
            using (HemsireServisi servis = new HemsireServisi())
            {
                if (servis.TekGetir(d => d.tc_no == tc) != null)
                    return false;
            }
            using (GorevliServisi servis = new GorevliServisi())
            {
                if (servis.TekGetir(d => d.tc_no == tc) != null)
                    return false;
            }
            return result;
        }
        public Kullanici KullaniciAl(int id)
        {
            using (DoktorServisi servis = new DoktorServisi())
            {
                if (servis.TekGetir(id) != null)
                    return servis.TekGetir(id);
            }
            using (HemsireServisi servis = new HemsireServisi())
            {
                if (servis.TekGetir(id) != null)
                    return servis.TekGetir(id);
            }
            using (GorevliServisi servis = new GorevliServisi())
            {
                if (servis.TekGetir(id) != null)
                    return servis.TekGetir(id);
            }
            return null;
        }
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
