﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class OdaServisi : IServis<odalar>
    {
        TTSEntities Depo;
        public OdaServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(odalar nesne)
        {
            Depo.odalar.Attach(nesne);
            DbEntityEntry<odalar> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(odalar nesne)
        {
            nesne.aktif_mi = true;
            Depo.odalar.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            odalar nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(odalar nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public odalar TekGetir(int id)
        {
            return Depo.odalar.Find(id);
        }

        public odalar TekGetir(Expression<Func<odalar, bool>> sorgu)
        {
            return Depo.odalar.FirstOrDefault(sorgu);
        }

        public IQueryable<odalar> TumunuGetir()
        {
            return Depo.odalar.Where(x => x.aktif_mi == true);
        }
    }
}
