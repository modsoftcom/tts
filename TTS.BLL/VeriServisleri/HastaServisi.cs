﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class HastaServisi : IServis<hastalar>
    {
        TTSEntities Depo;
        public HastaServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(hastalar nesne)
        {
            Depo.hastalar.Attach(nesne);
            DbEntityEntry<hastalar> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(hastalar nesne)
        {
            nesne.aktif_mi = true;
            Depo.hastalar.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            hastalar nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(hastalar nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public hastalar TekGetir(int id)
        {
            return Depo.hastalar.Find(id);
        }

        public hastalar TekGetir(Expression<Func<hastalar, bool>> sorgu)
        {
            return Depo.hastalar.FirstOrDefault(sorgu);
        }

        public IQueryable<hastalar> TumunuGetir()
        {
            return Depo.hastalar.Where(x => x.aktif_mi == true);
        }
    }
}
