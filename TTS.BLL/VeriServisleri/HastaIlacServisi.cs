﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class HastaIlacServisi : IServis<hastanin_ilaclari>
    {
        TTSEntities Depo;
        public HastaIlacServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(hastanin_ilaclari nesne)
        {
            Depo.hastanin_ilaclari.Attach(nesne);
            DbEntityEntry<hastanin_ilaclari> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(hastanin_ilaclari nesne)
        {
            nesne.aktif_mi = true;
            Depo.hastanin_ilaclari.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            hastanin_ilaclari nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(hastanin_ilaclari nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public hastanin_ilaclari TekGetir(int id)
        {
            return Depo.hastanin_ilaclari.Find(id);
        }

        public hastanin_ilaclari TekGetir(Expression<Func<hastanin_ilaclari, bool>> sorgu)
        {
            return Depo.hastanin_ilaclari.FirstOrDefault(sorgu);
        }

        public IQueryable<hastanin_ilaclari> TumunuGetir()
        {
            return Depo.hastanin_ilaclari.Where(x => x.aktif_mi == true);
        }
    }
}
