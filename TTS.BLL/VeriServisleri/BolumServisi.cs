﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class BolumServisi : IServis<bolumler>
    {
        TTSEntities Depo;
        public BolumServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(bolumler nesne)
        {
            Depo.bolumler.Attach(nesne);
            DbEntityEntry<bolumler> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(bolumler nesne)
        {
            nesne.aktif_mi = true;
            Depo.bolumler.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            bolumler bolum = TekGetir(id);
            bolum.aktif_mi = false;
            Duzenle(bolum);
        }

        public void Sil(bolumler nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public bolumler TekGetir(int id)
        {
            return Depo.bolumler.Find(id);
        }

        public bolumler TekGetir(Expression<Func<bolumler, bool>> sorgu)
        {
            return Depo.bolumler.FirstOrDefault(sorgu);
        }

        public IQueryable<bolumler> TumunuGetir()
        {
            return Depo.bolumler.Where(x => x.aktif_mi == true);
        }
    }
}
