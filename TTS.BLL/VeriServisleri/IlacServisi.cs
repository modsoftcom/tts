﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class IlacServisi : IServis<ilaclar>
    {
        TTSEntities Depo;
        public IlacServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(ilaclar nesne)
        {
            Depo.ilaclar.Attach(nesne);
            DbEntityEntry<ilaclar> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(ilaclar nesne)
        {
            nesne.aktif_mi = true;
            Depo.ilaclar.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            ilaclar nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(ilaclar nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public ilaclar TekGetir(int id)
        {
            return Depo.ilaclar.Find(id);
        }

        public ilaclar TekGetir(Expression<Func<ilaclar, bool>> sorgu)
        {
            return Depo.ilaclar.FirstOrDefault(sorgu);
        }

        public IQueryable<ilaclar> TumunuGetir()
        {
            return Depo.ilaclar.Where(x => x.aktif_mi == true);
        }
    }
}
