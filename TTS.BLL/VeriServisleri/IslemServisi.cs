﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class IslemServisi : IServis<islemler>
    {
        TTSEntities Depo;
        public IslemServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(islemler nesne)
        {
            Depo.islemler.Attach(nesne);
            DbEntityEntry<islemler> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(islemler nesne)
        {
            nesne.aktif_mi = true;
            Depo.islemler.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            islemler nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(islemler nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public islemler TekGetir(int id)
        {
            return Depo.islemler.Find(id);
        }

        public islemler TekGetir(Expression<Func<islemler, bool>> sorgu)
        {
            return Depo.islemler.FirstOrDefault(sorgu);
        }

        public IQueryable<islemler> TumunuGetir()
        {
            return Depo.islemler.Where(x => x.aktif_mi == true);
        }
    }
}
