﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class NobetServisi : IServis<nobetler>
    {
        TTSEntities Depo;
        public NobetServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(nobetler nesne)
        {
            Depo.nobetler.Attach(nesne);
            DbEntityEntry<nobetler> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(nobetler nesne)
        {
            nesne.aktif_mi = true;
            Depo.nobetler.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            nobetler nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(nobetler nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public nobetler TekGetir(int id)
        {
            return Depo.nobetler.Find(id);
        }

        public nobetler TekGetir(Expression<Func<nobetler, bool>> sorgu)
        {
            return Depo.nobetler.FirstOrDefault(sorgu);
        }

        public bool BugunNobetciMi(int id)
        {
            var bugunkuNobetler = Depo.nobetler.Where(n =>
                 System.Data.Entity.SqlServer.SqlFunctions.DateDiff("DAY", n.tarih, DateTime.Now) == 0);
            return bugunkuNobetler.Count(n => n.hemsire_id == id) > 0;
        }
        public IQueryable<nobetler> TumunuGetir()
        {
            return Depo.nobetler.Where(x => x.aktif_mi == true);
        }
    }
}
