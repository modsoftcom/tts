﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class DoktorServisi : IServis<doktorlar>, IDisposable
    {
        private TTSEntities Depo;
        public DoktorServisi()
        {
            Depo = new TTSEntities();
        }

        public void Duzenle(doktorlar nesne)
        {
            Depo.doktorlar.Attach(nesne);
            DbEntityEntry<doktorlar> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(doktorlar nesne)
        {
            nesne.aktif_mi = true;
            Depo.doktorlar.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            doktorlar doktor = TekGetir(id);
            doktor.aktif_mi = false;
            Duzenle(doktor);
        }

        public void Sil(doktorlar nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public doktorlar TekGetir(int id)
        {
            return Depo.doktorlar.Find(id);
        }

        public IQueryable<doktorlar> TumunuGetir()
        {
            return Depo.doktorlar.Where(x => x.aktif_mi == true);
        }

        public void Dispose()
        {
            Depo.Dispose();
        }

        public doktorlar TekGetir(Expression<Func<doktorlar, bool>> sorgu)
        {
            return Depo.doktorlar.FirstOrDefault(sorgu);
        }
    }
}
