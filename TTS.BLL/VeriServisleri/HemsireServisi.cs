﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class HemsireServisi : IServis<hemsireler>
    {
        TTSEntities Depo;
        public HemsireServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(hemsireler nesne)
        {
            Depo.hemsireler.Attach(nesne);
            DbEntityEntry<hemsireler> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(hemsireler nesne)
        {
            nesne.aktif_mi = true;
            Depo.hemsireler.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            hemsireler nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(hemsireler nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public hemsireler TekGetir(int id)
        {
            return Depo.hemsireler.Find(id);
        }

        public hemsireler TekGetir(Expression<Func<hemsireler, bool>> sorgu)
        {
            return Depo.hemsireler.FirstOrDefault(sorgu);
        }

        public IQueryable<hemsireler> TumunuGetir()
        {
            return Depo.hemsireler.Where(x => x.aktif_mi == true);
        }
    }
}
