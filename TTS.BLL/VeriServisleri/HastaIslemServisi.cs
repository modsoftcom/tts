﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class HastaIslemServisi : IServis<hastanin_islemleri>
    {
        TTSEntities Depo;
        public HastaIslemServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(hastanin_islemleri nesne)
        {
            Depo.hastanin_islemleri.Attach(nesne);
            DbEntityEntry<hastanin_islemleri> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(hastanin_islemleri nesne)
        {
            nesne.aktif_mi = true;
            Depo.hastanin_islemleri.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            hastanin_islemleri nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(hastanin_islemleri nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public hastanin_islemleri TekGetir(int id)
        {
            return Depo.hastanin_islemleri.Find(id);
        }

        public hastanin_islemleri TekGetir(Expression<Func<hastanin_islemleri, bool>> sorgu)
        {
            return Depo.hastanin_islemleri.FirstOrDefault(sorgu);
        }

        public IQueryable<hastanin_islemleri> TumunuGetir()
        {
            return Depo.hastanin_islemleri.Where(x => x.aktif_mi == true);
        }
    }
}
