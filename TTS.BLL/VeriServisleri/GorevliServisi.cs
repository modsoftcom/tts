﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TTS.DAL;

namespace TTS.BLL
{
    public class GorevliServisi : IServis<gorevliler>
    {
        TTSEntities Depo;
        public GorevliServisi()
        {
            Depo = new TTSEntities();
        }
        public void Dispose()
        {
            Depo.Dispose();
        }

        public void Duzenle(gorevliler nesne)
        {
            Depo.gorevliler.Attach(nesne);
            DbEntityEntry<gorevliler> entry = Depo.Entry(nesne);
            entry.State = EntityState.Modified;
            Depo.SaveChanges();
        }

        public void Ekle(gorevliler nesne)
        {
            nesne.aktif_mi = true;
            Depo.gorevliler.Add(nesne);
            Depo.SaveChanges();
        }

        public void Sil(int id)
        {
            gorevliler nesne = TekGetir(id);
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public void Sil(gorevliler nesne)
        {
            nesne.aktif_mi = false;
            Duzenle(nesne);
        }

        public gorevliler TekGetir(int id)
        {
            return Depo.gorevliler.Find(id);
        }

        public gorevliler TekGetir(Expression<Func<gorevliler, bool>> sorgu)
        {
            return Depo.gorevliler.FirstOrDefault(sorgu);
        }

        public IQueryable<gorevliler> TumunuGetir()
        {
            return Depo.gorevliler.Where(x => x.aktif_mi == true);
        }
    }
}
