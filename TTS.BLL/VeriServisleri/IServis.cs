﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTS.BLL
{
    interface IServis<T>:IDisposable
    {
        void Ekle(T nesne);
        T TekGetir(int id);
        T TekGetir(System.Linq.Expressions.Expression<Func<T, bool>> sorgu);
        IQueryable<T> TumunuGetir();
        void Duzenle(T nesne);
        void Sil(int id);
        void Sil(T nesne);
    }
}
